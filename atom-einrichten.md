# Atom einrichten

Fließendes Arbeiten mit Shortcuts.

* Schreiben mit Markdown
* Versionieren mit Git
* Programmieren mit Groovy


## Versionieren mit Git/GitLab

* Import Project: cmd-shift-i
* View Git-Tab: cmd-shift-g
* Commit:
* Fetch:
* Pull:
* Push:
* Stage all


## Texte mit Markdown schreiben

**Settings -> Install**
* markdown-preview-enhanced
* markdown-writer
* tool-bar-markdown-writer
* markdown-table-editor


## Programmieren mit Groovy

**Settings -> Install**
* language-groovy
* script

Shortcuts
* Run Skript: cmd-i



## Programmieren mit Python

**Settings -> Install**
* script
