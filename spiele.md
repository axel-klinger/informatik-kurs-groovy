# Ein paar Spiele

// Version 0.1

[TIC TAC TOE](https://raw.githubusercontent.com/axel-klinger/groovy-tictactoe/v0.1/README.md ':include')

[Repository auf GitHub](https://github.com/axel-klinger/groovy-tictactoe/tree/v0.1)

// Version HEAD

[TIC TAC TOE](https://raw.githubusercontent.com/axel-klinger/groovy-tictactoe/master/README.md ':include')

[Code](https://raw.githubusercontent.com/axel-klinger/groovy-tictactoe/master/TicTacToe.groovy  ':include :type=code')

[Repository auf GitHub](https://github.com/axel-klinger/groovy-tictactoe)



[HANOI](https://raw.githubusercontent.com/axel-klinger/groovy-hanoi/master/README.md ':include')

[HANGMAN](https://raw.githubusercontent.com/axel-klinger/groovy-hangman/master/README.md ':include')
