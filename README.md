# Informatik Kurs - Programmieren mit Groovy

Eine kleine Einführung in die Programmierung mit Groovy anhand einiger spielerischer Beispiele.

Referenzen
* [Kurs als Ebook](https://axel-klinger.gitlab.io/informatik-kurs-groovy/course.epub)
* [Kurs als PDF](https://axel-klinger.gitlab.io/informatik-kurs-groovy/course.pdf)
* [Kurs als HTML](https://axel-klinger.gitlab.io/informatik-kurs-groovy/course.html)
* [Kurs als LiaScript](https://liascript.github.io/course/?https://api.allorigins.win/raw?url=https://axel-klinger.gitlab.io/informatik-kurs-groovy/course.md)
* [Kurs als Docsify](https://axel-klinger.gitlab.io/informatik-kurs-groovy)
