# Graph

Mermaid
```mermaid
graph TD;
    HH-->H;
    H-->B;
    HH-->B;
    B-->H;
```

Graphviz
```dot
digraph G {
  HH -> H
  H -> B
  HH -> B
  B -> H
}
```
